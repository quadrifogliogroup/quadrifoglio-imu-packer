#include "ros/ros.h"
//#include <tf/transform_broadcaster.h>
#include "sensor_msgs/Imu.h"
#include "sensor_msgs/MagneticField.h"
#include "sensor_msgs/JointState.h"
#include "sensor_msgs/Joy.h"
#include "geometry_msgs/Twist.h"
#include "quadrifoglio_msgs/imuLean.h"
#include "quadrifoglio_msgs/rawJoints.h"
#include "quadrifoglio_msgs/rawJoy.h"

#define TODEG 57.29577951          //Conversion factors for degrees & radians
#define TORAD 0.017453293         //The AVR cannot reach this level of precision but w/e

class ImuPacker{
    public:
    ImuPacker(ros::Publisher* imuPub, ros::Publisher* magPub, ros::Publisher* jointPub, ros::Publisher* joyPub,
        ros::Publisher* twistPub,
        sensor_msgs::Imu &imuMsg, sensor_msgs::MagneticField &magMsg, sensor_msgs::JointState &jointMsg,
        sensor_msgs::Joy &joyMsg, double gfs, double afs, double mfs){

        _imuPub = imuPub;
        _imuMsg = imuMsg;

        _magPub = magPub;
        _magMsg = magMsg;

        _jointPub = jointPub;
        _jointMsg = jointMsg;

        _joyPub = joyPub;
        _joyMsg = joyMsg;

        _twistPub = twistPub;

        _gyroFullScale = gfs;  //Full scale range in degrees
        _accelFullScale = afs; //Full scale range in G
        _magFullScale = mfs; //Full scale range in gauss
        _imuMsg.header.frame_id = "imu";
        _magMsg.header.frame_id = "imu";
    }
    void ImuCallback(const quadrifoglio_msgs::imuLean::ConstPtr& imuLean){
        _imuMsg.header.stamp = imuLean->header.stamp;
        _imuMsg.angular_velocity.x = (imuLean->gx/32767.0)*_gyroFullScale*TORAD; //Rad/s
        _imuMsg.angular_velocity.y = (imuLean->gy/32767.0)*_gyroFullScale*TORAD;
        _imuMsg.angular_velocity.z = (imuLean->gz/32767.0)*_gyroFullScale*TORAD;
        _imuMsg.linear_acceleration.x = (imuLean->ax/32767.0)*_accelFullScale*9.81; //m/s²
        _imuMsg.linear_acceleration.y = (imuLean->ay/32767.0)*_accelFullScale*9.81;
        _imuMsg.linear_acceleration.z = (imuLean->az/32767.0)*_accelFullScale*9.81;
        
        _magMsg.header.stamp = imuLean->header.stamp;
        _magMsg.magnetic_field.x = (imuLean->mx/32767.0)*_gyroFullScale*0.0001;  //Tesla
        _magMsg.magnetic_field.y = (imuLean->my/32767.0)*_gyroFullScale*0.0001;
        _magMsg.magnetic_field.z = (imuLean->mz/32767.0)*_gyroFullScale*0.0001;

        _imuPub->publish(_imuMsg);
        _magPub->publish(_magMsg);
    }
    void PoseCallback(const quadrifoglio_msgs::rawJoints::ConstPtr& msg){
        _jointMsg.header.stamp = ros::Time::now();
        _jointMsg.position[0] = -msg->frontLeftAngle*TORAD;
        _jointMsg.position[1] = -msg->frontRightAngle*TORAD;
        _jointMsg.position[2] = -msg->rearLeftAngle*TORAD;
        _jointMsg.position[3] = -msg->rearRightAngle*TORAD;
        _jointMsg.position[4] = -msg->frontUltraAngle*TORAD;
        _jointMsg.position[5] = -msg->rearUltraAngle*TORAD;
        _jointPub->publish(_jointMsg);
    }
    void JoyCallback(const quadrifoglio_msgs::rawJoy::ConstPtr& msg){
        _joyMsg.header.stamp = ros::Time::now();
        _joyMsg.axes[0] = msg->ax1; _joyMsg.axes[1] = msg->ax2;
        _joyMsg.axes[2] = msg->ax3; _joyMsg.axes[3] = msg->ax4;
        _joyMsg.axes[4] = msg->ax5; _joyMsg.axes[5] = msg->ax6;
        _joyMsg.buttons[0] = (bool)(msg->buttons & 1); _joyMsg.buttons[1] = (bool)(msg->buttons & 1<<1);
        _joyMsg.buttons[2] = (bool)(msg->buttons & 1<<2); _joyMsg.buttons[3] = (bool)(msg->buttons & 1<<3);
        _joyMsg.buttons[4] = (bool)(msg->buttons & 1<<4); _joyMsg.buttons[5] = (bool)(msg->buttons & 1<<5);
        _joyMsg.buttons[6] = (bool)(msg->buttons & 1<<6); _joyMsg.buttons[7] = (bool)(msg->buttons & 1<<7);
        _joyPub->publish(_joyMsg);

        //Generate and publish a twist message out of the joystick data
        geometry_msgs::Twist twistMsg;
        twistMsg.linear.x = _joyMsg.axes[2];
        twistMsg.linear.y = -_joyMsg.axes[3]/2.0;  //half speed strafe
        twistMsg.angular.z = -_joyMsg.axes[1];
        _twistPub->publish(twistMsg);
    }
    private:
    ros::Publisher* _imuPub;
    ros::Publisher* _magPub;
    ros::Publisher* _jointPub;
    ros::Publisher* _joyPub;
    ros::Publisher* _twistPub;

    sensor_msgs::Imu _imuMsg;
    sensor_msgs::MagneticField _magMsg;
    sensor_msgs::JointState _jointMsg;
    sensor_msgs::Joy _joyMsg;

    double _gyroFullScale;  //Full scale range in degrees
    double _accelFullScale; //Full scale range in G
    double _magFullScale; //Full scale range in gauss
};


int main(int argc, char **argv){
    ros::init(argc, argv, "quadrifoglio_imu_packer");
    ros::NodeHandle n;

    ros::Publisher imuPub = n.advertise<sensor_msgs::Imu>("imu/data_raw", 1);
    ros::Publisher magPub = n.advertise<sensor_msgs::MagneticField>("imu/mag", 1);
    ros::Publisher jointPub = n.advertise<sensor_msgs::JointState>("joint_states",1);
    ros::Publisher joyPub = n.advertise<sensor_msgs::Joy>("joy",1);
    ros::Publisher twistPub = n.advertise<geometry_msgs::Twist>("cmd_vel_joy",1);

    sensor_msgs::Imu imuMsg;
    sensor_msgs::MagneticField magMsg;
    sensor_msgs::JointState jointMsg;
    sensor_msgs::Joy joyMsg;
    
    jointMsg.name.resize(6);
    jointMsg.position.resize(6);
    jointMsg.velocity.resize(0);
    jointMsg.effort.resize(0);
    jointMsg.name[0] = "frontLeftWheelAngle";
    jointMsg.name[1] = "frontRightWheelAngle";
    jointMsg.name[2] = "rearLeftWheelAngle";
    jointMsg.name[3] = "rearRightWheelAngle";
    jointMsg.name[4] = "base_to_f_ultra";
    jointMsg.name[5] = "base_to_r_ultra";

    joyMsg.axes.resize(6);
    joyMsg.buttons.resize(8);

    ImuPacker imuPacker(&imuPub, &magPub, &jointPub, &joyPub, &twistPub,
                        imuMsg, magMsg, jointMsg, joyMsg, 500, 2, 4.91505);

    ros::Subscriber imuLeanSub = n.subscribe("quadrifoglio/imu_lean", 10, &ImuPacker::ImuCallback, &imuPacker);
    ros::Subscriber jointSub = n.subscribe("quadrifoglio/rawJoints", 1, &ImuPacker::PoseCallback, &imuPacker);
    ros::Subscriber joySub = n.subscribe("quadrifoglio/rawJoy", 1, &ImuPacker::JoyCallback, &imuPacker);
    
    ros::spin();

    return 0;
}
